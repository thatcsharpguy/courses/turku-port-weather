# Predicting rain conditions at a Finnish port

Using the *Instantaneous Observations* downloaded from [https://en.ilmatieteenlaitos.fi/download-observations](https://en.ilmatieteenlaitos.fi/download-observations), let's try to predict what the weather conditions will be at a port city of Turku in Finland.

The observations come from the **Turku Artukainen** station, and are divided between two timespans:

 - January 1, 2020 12:00 AM — July 31, 2021 11:59 PM
 - July 31, 2021 12:00 AM — October 31, 2021 11:59 PM
